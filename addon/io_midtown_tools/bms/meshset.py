﻿# ##### BEGIN GPL LICENSE BLOCK #####
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# Copyright (C) 2016: Mark Ludwig

import sys
import os
import io

import struct

from io_midtown_tools.bms.types import *

BMS_MAGIC = 0x4D534833 # '3HSM'

class BMSFlags:
    Normals     = (1 << 0)
    TexCoords   = (1 << 1)
    Colors      = (1 << 2)
    Unknown     = (1 << 3)
    CPVs        = (1 << 4)

    Invalid     = 0x60

class BMSMaterialFlags:
    Color       = (1 << 0)
    SWrap       = (1 << 1)
    TWrap       = (1 << 2)
    
    Flag4       = (1 << 3)
    Flag5       = (1 << 4)
    Flag6       = (1 << 5)
    Flag7       = (1 << 6)
    Flag8       = (1 << 7)

    Invalid     = 0x7F00

class Adjunct:
    position            = Vector3()
    texture_coordinates = Vector2()
    smoothing_group     = 0
    color               = Color()

class Material:
    texture             = ''
    flags               = 0
    reserved            = 0
    shininess           = 0.0
    diffuse             = Color()

class Element:
    material            = Material()
    indices             = []

class MeshSet:
    name                = "MeshSet"

    offset              = Vector3()
    origin              = Vector3()

    flags               = 0

    vertices            = []
    materials           = []
    cpvs                = []
    elements            = []

class BMSFileIO:
    __file              = None

    def __init__(self, file):
        if not isinstance(file, io.BufferedReader):
            raise TypeError("%s: Must be initialized with BufferedReader! (Got type: %s)" % (self.__class__.__name__, file.__class__.__name__))

        self.__file = file

    def file(self):
        return self.__file

class BMSReader(BMSFileIO):
    def read_color(self):
        r,g,b,a = struct.unpack('BBBB', self.file().read(4))
        return Color(r,g,b,a)

    def read_vector2(self):
        x,y = struct.unpack('2f', self.file().read(8))
        return Vector2(x,y)

    def read_vector3(self):
        x,y,z = struct.unpack('3f', self.file().read(12))
        return Vector3(x,y,z)

    def read_vector4(self):
        x,y,z,w = struct.unpack('4f', self.file().read(16))
        return Vector4(x,y,z,w)

class BinaryMeshSet(MeshSet):
    def binary_load(self, val):
        if val == None:
            return False

        file = None
        closeF = False

        if isinstance(val, str):
            if not os.path.isfile(val):
                raise FileNotFoundError("BMS File '%s' not found!" % val)
            file = io.open(val, 'rb')
            closeF = True
        if isinstance(val, io.BufferedReader):
            file = val

        reader = BMSReader(file)

        if (struct.unpack('i', file.read(4))[0] != BMS_MAGIC):
            raise IOError("Bad magic, cannot load BMS file!")

        self.offset = reader.read_vector3()

        nVertices, nAdjuncts, nElements, nIndices = struct.unpack('iiii', file.read(16))

        self.origin = reader.read_vector3()

        nMaterials, self.flags = struct.unpack('BB', file.read(2))

        # skip the next 6 bytes
        file.seek(6, io.SEEK_CUR)

        # read materials
        if nMaterials > 0:
            self.materials = [None] * nMaterials

            for m in range(nMaterials):
                mat = Material()

                m_buf = struct.unpack('32s ii f BBBB', file.read(0x30))

                mat.texture     = m_buf[0].decode('utf-8').rstrip('\0')
                mat.flags       = m_buf[1]

                mat.reserved    = m_buf[2]
                mat.shininess   = m_buf[3]

                mat.diffuse     = Color(r = m_buf[4],
                                        g = m_buf[5],
                                        b = m_buf[6],
                                        a = m_buf[7])

                self.materials[m] = mat

        ###############################
        # First pass
        ###############################

        # check if we need to read 8 extra vertices
        if nVertices >= 16:
            nVertices += 8

        vertices    = []
        normals     = []
        texCoords   = []
        colors      = []
        adjuncts    = []

        elements    = []
        indices     = []

        if (self.flags & BMSFlags.Invalid) != 0:
            raise IOError("Unknown flags '%d' -- cannot load BMS file!" % self.flags)

        # Vertices
        vertices = [None] * nVertices
        for v in range(nVertices):
            vertices[v] = reader.read_vector3()

        # Normals
        if (self.flags & BMSFlags.Normals) != 0:
            normals = [0] * nAdjuncts
            normals_buffer = file.read(1 * nAdjuncts)
            for n in range(nAdjuncts):
                normals[n] = struct.unpack_from('B', normals_buffer, n)[0]

        # TexCoords
        if (self.flags & BMSFlags.TexCoords) != 0:
            texCoords = [None] * nAdjuncts
            for i in range(nAdjuncts):
                texCoords[i] = reader.read_vector2()

        # Colors
        if (self.flags & BMSFlags.Colors) != 0:
            colors = [None] * nAdjuncts
            for c in range(nAdjuncts):
                colors[c] = reader.read_color()

        # Adjuncts
        adjuncts = [0] * nAdjuncts
        adjuncts_buffer = file.read(2 * nAdjuncts)
        for i in range(nAdjuncts):
            adjuncts[i] = struct.unpack_from('H', adjuncts_buffer, i)[0]

        # CPVs
        if (self.flags & BMSFlags.CPVs) != 0:
            self.cpvs = [None] * nElements

            for i in range(nElements):
                self.cpvs[i] = reader.read_vector4()

        # Elements
        elements = [0] * nElements
        elements_buffer = file.read(1 * nElements)
        for e in range(nElements):
            elements[e] = struct.unpack_from('B', elements_buffer, e)[0]

        # Indices
        indices = [0] * nIndices
        indices_buffer = file.read(2 * nIndices)
        for i in range(nIndices):
            indices[i] = struct.unpack_from('H', indices_buffer, i)[0]

        print('Parsed BMS file to 0x%X' % file.tell())

        ###############################
        # Second pass
        ###############################

        # compile adjuncts
        self.vertices = [None] * nAdjuncts

        for i in range(nAdjuncts):
            idx = adjuncts[i]

            adj = Adjunct()

            adj.position            = vertices[idx]
            adj.texture_coordinates = texCoords[i]
            adj.smoothing_group     = normals[i]
            adj.color               = colors[i]

            self.vertices[i] = adj

        # compile elements
        self.elements = [None] * nElements

        matId = -1
        elemLen = 0

        startIndex = 0
        lastElement = False

        for i in range(nElements):
            idx = elements[i]

            if i == (nElements - 1):
                lastElement = True
                elemLen += 4

            if idx != matId or lastElement:
                if i > 0:
                    # YOU'RE OUT OF YOUR ELEMENT, DONNY!
                    elem = Element()

                    elem.material = self.materials[matId - 1] if matId > 0 else None
                    elem.indices = [0] * elemLen

                    for t in range(elemLen):
                        elem.indices[t] = indices[startIndex + t]

                    startIndex += elemLen
                    elemLen = 0

                    self.elements[i] = elem

                matId = idx

            elemLen += 4

        # are we in charge of closing the file?
        if closeF:
            file.close()

        print('Finished loading BMS file!')

    def __init__(self,val=None):
        if val != None:
            self.binary_load(val)