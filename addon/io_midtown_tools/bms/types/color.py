# ##### BEGIN GPL LICENSE BLOCK #####
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# Copyright (C) 2016: Mark Ludwig

class Color:
    r = 0.0
    g = 0.0
    b = 0.0
    a = 1.0

    def __init__(self,
                 r=0.0,
                 g=0.0,
                 b=0.0,
                 a=1.0):
        def get_color(val):
            if isinstance(val, float):
                return val
            if isinstance(val, int):
                return (val / 255.0)

            raise TypeError('bad color type: %s' % type(val))

        self.r = get_color(r)
        self.g = get_color(g)
        self.b = get_color(b)
        self.a = get_color(a)

    def __str__(self):
        return '[%.4f, %.4f, %.4f, %.4f]' % (self.r, self.g, self.b, self.a)