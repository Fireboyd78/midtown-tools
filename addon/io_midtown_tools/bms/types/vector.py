﻿# ##### BEGIN GPL LICENSE BLOCK #####
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# Copyright (C) 2016: Mark Ludwig

class VectorObject:
    def copy(self):
        raise NotImplementedError()

    def add(self, other):
        raise NotImplementedError()
    def sub(self, other):
        raise NotImplementedError()
    def mul(self, other):
        raise NotImplementedError()
    def div(self, other):
        raise NotImplementedError()

    def __add__(self, other):
        return self.copy().add(other)
    def __sub__(self, other):
        return self.copy().sub(other)
    def __mul__(self, other):
        return self.copy().mul(other)
    def __div__(self, other):
        return self.copy().div(other)

    def __iadd__(self, other):
        return self.add(other)
    def __isub__(self, other):
        return self.sub(other)
    def __imul__(self, other):
        return self.mul(other)
    def __idiv__(self, other):
        return self.div(other)

class Vector2(VectorObject):
    x = 0.0
    y = 0.0

    def __init__(self,
                 x:float=0.0,
                 y:float=0.0):
        self.x = x
        self.y = y

    def __str__(self):
        return '[%.8f, %.8f]' % (self.x, self.y)

    def copy(self):
        return Vector2(self.x, self.y)

    def add(self, other):
        if isinstance(other, Vector2):
            self.x += other.x
            self.y += other.y
            return self
        else:
            raise NotImplementedError()

    def sub(self, other):
        if isinstance(other, Vector2):
            self.x -= other.x
            self.y -= other.y
            return self
        else:
            raise NotImplementedError()

    def mul(self, other):
        if isinstance(other, Vector2):
            self.x *= other.x
            self.y *= other.y
            return self
        else:
            raise NotImplementedError()

    def div(self, other):
        if isinstance(other, Vector2):
            self.x /= other.x
            self.y /= other.y
            return self
        else:
            raise NotImplementedError()

    def __eq__(self, other):
        if isinstance(other, Vector2):
            return ((self.x == other.x) and (self.y == other.y))
        return False

class Vector3(VectorObject):
    x = 0.0
    y = 0.0
    z = 0.0

    def __init__(self,
                 x:float=0.0,
                 y:float=0.0,
                 z:float=0.0):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return '[%.8f, %.8f, %.8f]' % (self.x, self.y, self.z)

    def copy(self):
        return Vector3(self.x, self.y, self.z)

    def add(self, other):
        if isinstance(other, Vector3):
            self.x += other.x
            self.y += other.y
            self.z += other.z
            return self
        else:
            raise NotImplementedError()

    def sub(self, other):
        if isinstance(other, Vector3):
            self.x -= other.x
            self.y -= other.y
            self.z -= other.z
            return self
        else:
            raise NotImplementedError()

    def mul(self, other):
        if isinstance(other, Vector3):
            self.x *= other.x
            self.y *= other.y
            self.z *= other.z
            return self
        else:
            raise NotImplementedError()

    def div(self, other):
        if isinstance(other, Vector3):
            self.x /= other.x
            self.y /= other.y
            self.z /= other.z
            return self
        else:
            raise NotImplementedError()

    def __eq__(self, other):
        if isinstance(other, Vector3):
            return ((self.x == other.x) and (self.y == other.y) and (self.z == other.z))
        return False

class Vector4(VectorObject):
    x = 0.0
    y = 0.0
    z = 0.0
    w = 0.0

    def __init__(self,
                 x:float=0.0,
                 y:float=0.0,
                 z:float=0.0,
                 w:float=0.0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def __str__(self):
        return '[%.8f, %.8f, %.8f, %.8f]' % (self.x, self.y, self.z, self.w)

    def copy(self):
        return Vector4(self.x, self.y, self.z, self.w)

    def add(self, other):
        if isinstance(other, Vector4):
            self.x += other.x
            self.y += other.y
            self.z += other.z
            self.w += other.w
            return self
        else:
            raise NotImplementedError()

    def sub(self, other):
        if isinstance(other, Vector4):
            self.x -= other.x
            self.y -= other.y
            self.z -= other.z
            self.w -= other.w
            return self
        else:
            raise NotImplementedError()

    def mul(self, other):
        if isinstance(other, Vector4):
            self.x *= other.x
            self.y *= other.y
            self.z *= other.z
            self.w *= other.w
            return self
        else:
            raise NotImplementedError()

    def div(self, other):
        if isinstance(other, Vector4):
            self.x /= other.x
            self.y /= other.y
            self.z /= other.z
            self.w /= other.w
            return self
        else:
            raise NotImplementedError()

    def __eq__(self, other):
        if isinstance(other, Vector4):
            return ((self.x == other.x) and (self.y == other.y) and (self.z == other.z) and (self.w == other.w))
        return False